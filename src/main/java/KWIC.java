import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;

public class KWIC {

    public ArrayList<String> readIgnoreWords(ArrayList<String> ans) {
        int seperator = ans.indexOf("::");
        ArrayList<String> IgnoreWords = new ArrayList<String>();
        for(int i = 0; i < seperator; i++){
            IgnoreWords.add(ans.get(i));
        }
        return IgnoreWords;
    }

    public ArrayList<String> readTitles(ArrayList<String> ans) {
        int seperator = ans.indexOf("::");
        int inputSize = ans.size();
        ArrayList<String> Titles = new ArrayList<String>();
        for(int i = seperator + 1; i < inputSize ; i++){
            Titles.add(ans.get(i));
        }
        return Titles;
    }


    public ArrayList<String> splitTitles(ArrayList<String> ans) {
        ArrayList<String> words = new ArrayList<String>();
        for(int i = 0; i < ans.size();i++) {
            String title = ans.get(i);
            String word[] = title.split(" ");
            for(int j = 0; j < word.length; j++) {
                words.add(word[j]);
            }
        }
        return words;
    }

    public ArrayList<String> getKeyWords(ArrayList<String> words, ArrayList<String> ignores) {
        for(int i=0;i<ignores.size();i++){
            String b=ignores.get(i);
            words.removeIf(s -> s.equalsIgnoreCase(b));
        }
        return words;
    }

    public ArrayList<String> sortKeys(ArrayList<String> words) {
        HashSet<String> wordSet = new HashSet<String>();

        words.forEach(value -> { wordSet.add(value);});
        ArrayList<String> sortWords = new ArrayList<String>(wordSet);
        Collections.sort(sortWords);

        return sortWords;
    }

    public String capital(String word, String title) {
        int index = 0;
        String capTitle = "";
        while((index=title.indexOf(word,index))!=-1){
            StringBuilder capTitleBuilder=new StringBuilder();
            capTitleBuilder.append(title);
            capTitleBuilder.replace(index,index+word.length(), word.toUpperCase());
            capTitle=capTitle + capTitleBuilder.toString()+"\n";
            index = index + word.length();
        }
        return capTitle;
    }

    public ArrayList<String> lower(ArrayList<String> ans) {
        ans.replaceAll(String::toLowerCase);

        return ans;
    }

    public static String KWICfunc(ArrayList<String> Input) {
        ArrayList<String> output = new ArrayList<String>();

        ArrayList<String> input = new KWIC().lower(Input);
        ArrayList<String> ignore = new KWIC().readIgnoreWords(input);
        ArrayList<String> title = new KWIC().readTitles(input);
        ArrayList<String> keys = new KWIC().splitTitles(title);
        ArrayList<String> ckey = new KWIC().getKeyWords(keys, ignore);
        ArrayList<String> skey = new KWIC().sortKeys(ckey);

        for (int i=0; i<skey.size();i++) {
            String word = skey.get(i);
            for (int j = 0; j < title.size(); j++) {
                String titleLine = title.get(j);
                boolean status = titleLine.contains(word);
                if (status) {
                    output.add(new KWIC().capital(word, titleLine));
                }
            }
        }

        String outputString = new String();
        for (int i = 0; i < output.size(); i++){
            outputString = outputString + output.get(i);
        }

        return outputString;
    }

    public static void main(String[] args) {
        ArrayList<String> input = new ArrayList<String>();
        Scanner scan = new Scanner(System.in);

        while (scan.hasNextLine()) {
            String str = scan.nextLine();
            if (str.equals("")) {
                break;
            }
            input.add(str);
        }
        scan.close();

        String output = KWIC.KWICfunc(input);

        System.out.println(output);

    }
}




