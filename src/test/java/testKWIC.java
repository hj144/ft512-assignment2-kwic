import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Spliterators;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class testKWIC {
    public ArrayList<String> getTestList() {
        ArrayList<String> ans = new ArrayList<>();
        ans.add("is" );
        ans.add("the" );
        ans.add("of" );
        ans.add("and" );
        ans.add("as" );
        ans.add("a" );
        ans.add("but" );
        ans.add("::" );
        ans.add("Descent of Man" );
        ans.add("The Ascent of Man" );
        ans.add("The Old Man and The Sea" );
        ans.add("A Portrait of The Artist As a Young Man" );
        ans.add("A Man is a Man but Bubblesort IS A DOG" );
        return ans;
    }

    public String getOutputList() {
        String expected = "a portrait of the ARTIST as a young man\n"
                + "the ASCENT of man\n"
                + "a man is a man but BUBBLESORT is a dog\n"
                + "DESCENT of man\n"
                + "a man is a man but bubblesort is a DOG\n"
                + "descent of MAN\n"
                + "the ascent of MAN\n"
                + "the old MAN and the sea\n"
                + "a portrait of the artist as a young MAN\n"
                + "a MAN is a man but bubblesort is a dog\n"
                + "a man is a MAN but bubblesort is a dog\n"
                + "the OLD man and the sea\n"
                + "a PORTRAIT of the artist as a young man\n"
                + "the old man and the SEA\n"
                + "a portrait of the artist as a YOUNG man\n";

        return expected;
    }

    @Test
    void testReadIngoreWords() {
        ArrayList<String> ans = getTestList();
        ArrayList<String> expected = new ArrayList<>();
        expected.add("is");
        expected.add("the");
        expected.add("of");
        expected.add("and");
        expected.add("as");
        expected.add("a");
        expected.add("but");

        assertEquals(new KWIC().readIgnoreWords(ans), expected);
    }

    @Test
    void testReadTitles() {
        ArrayList<String> ans = getTestList();
        ArrayList<String> expected = new ArrayList<>();
        expected.add("Descent of Man" );
        expected.add("The Ascent of Man" );
        expected.add("The Old Man and The Sea" );
        expected.add("A Portrait of The Artist As a Young Man" );
        expected.add("A Man is a Man but Bubblesort IS A DOG" );

        assertEquals(new KWIC().readTitles(ans), expected);
    }

    @Test
    void testSplitTitles() {
        ArrayList<String> expected = new ArrayList<>();
        ArrayList<String> titles = new ArrayList<>();
        titles.add("Descent of Man" );
        titles.add("The Ascent of Man" );
        titles.add("The Old Man and The Sea" );
        expected.add("Descent");
        expected.add("of");
        expected.add("Man");
        expected.add("The");
        expected.add("Ascent");
        expected.add("of");
        expected.add("Man");
        expected.add("The");
        expected.add("Old");
        expected.add("Man");
        expected.add("and");
        expected.add("The");
        expected.add("Sea");

        assertEquals(new KWIC().splitTitles(titles), expected);
    }

    @Test
    void testGetKeyWords() {
        ArrayList<String> words = new ArrayList<String>();
        ArrayList<String> ignores = new ArrayList<String>();
        ArrayList<String> expected = new ArrayList<String>();
        words.add("Descent");
        words.add("of");
        words.add("Man");
        ignores.add("of");
        ignores.add("is");
        expected.add("Descent");
        expected.add("Man");

        assertEquals(new KWIC().getKeyWords(words,ignores), expected);


    }

    @Test
    void testSortKeys() {
        ArrayList<String> words = new ArrayList<String>();
        ArrayList<String> expected = new ArrayList<String>();
        words.add("Descent");
        words.add("Man");
        words.add("Descent");
        words.add("Apple");
        expected.add("Apple");
        expected.add("Descent");
        expected.add("Man");

        assertEquals(new KWIC().sortKeys(words), expected);
    }

    @Test
    void testCapital() {
        String word="man";
        String title="a man is a man but bubblesort is a dog";
        String result= new KWIC().capital(word,title);
        String expected="a MAN is a man but bubblesort is a dog"+"\n"
                +"a man is a MAN but bubblesort is a dog"+"\n";

        assertEquals(result,expected);
    }

    @Test
    void testLower() {
        ArrayList<String> ans = new ArrayList<String>();
        ArrayList<String> expected = new ArrayList<String>();
        ans.add("A CAPITAL TITLE");
        ans.add("THE SECOND CAPITAL TITLE");
        expected.add("a capital title");
        expected.add("the second capital title");

        assertEquals(new KWIC().lower(ans), expected);
    }

    @Test
    void testKWICfun() {
        ArrayList<String> ans = getTestList();
        String expected = getOutputList();

        assertEquals(new KWIC().KWICfunc(ans), expected);
    }
}
